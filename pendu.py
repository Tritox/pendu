#!/usr/bin/python

from __future__ import print_function
import random

# Initialisation de la liste des mots 
pendu_mots = []

# Ouverture du fichier pendu
with open("pendu.txt") as pendu:
    # Lecture ligne par ligne du contenu du fichier
    for line in pendu.readlines():
        # Ajout de la ligne dans la liste des mots
        pendu_mots.append(line.rstrip())

# Affichage de la liste des mots

index_max = len(pendu_mots)-1

# Donne un index aleatoire en 0 et index_max
mot = random.randrange(0, index_max)
mot_a_trouver = pendu_mots[mot]

print(pendu_mots[mot])
# Affiche le mot choisi

# Affecter un ditcionnaire pour les lettres rentrees par l'utilisateur
lettres = []


# Affecter une variable au nombre d'essais du debut
essais = 10

# creation d'un compteur de lettre
mot_a_trouver_len = len(mot_a_trouver)
foundletter_count = 0

while True:

    # Considerons que on va trouver le mot
    found = False

    # Pour chaque lettre du mot a trouver
    for lettre in mot_a_trouver:
        # Si la lettre est dans le mot, l'afficher
        if lettre.lower() in ''.join(lettres).lower():
            print(lettre, end='')
        # Sinon afficher un tiret
        else:
            
            print("-", end='')
    print()

    print("Vous avez le droit a {0} erreurs ".format(essais))
    # Assigner la lettre de l'utilisateur
    lettre_choisie = raw_input("Choisir une lettre : ")

    # Si l'utilisateur choisie plusieurs lettres
    if  len(lettre_choisie) != 1:
        # Dire qu'il faut choisir une seule lettre
        print('Il faut choisir une seule lettre ')
        continue
    # Si la lettre de l'utilisateur est dans les lettres affichees du mot
    if lettre_choisie.lower() in "".join(lettres).lower():
        # Dire que la lettre a deja ete choisie
        print("La lettre a deja ete choisie")
        continue

    # Ajouter la lettre dans la liste "lettres"
    lettres.append(lettre_choisie)

    # Si la lettre choisis est dans le mot a trouver
    if lettre_choisie.lower() in mot_a_trouver.lower():
        count_letter = 0
        for _letter in mot_a_trouver.lower():
            if lettre_choisie.lower() == _letter:
                # Ajouter 1 au compteur de lettre
                foundletter_count += 1
        found = True

    # Si une lettre n'est pas trouve    
    if not found:
        # Enlever un essai
        essais -= 1

    # Si il n'y a plus d'essai restant
    if essais == 0:
        # Dire que j'ai perdu
        print("j'ai perdu")
        break

    # Si le compteur de lettre est egale au nombre de lettre du mot a trouver
    print(foundletter_count, mot_a_trouver_len)     
    if foundletter_count == mot_a_trouver_len:
        # Dire que j'ai gange
        print("J'ai gagne")
        break