# int = Nombre entier
# float = Nombre relatif
# list = []
# tuple = () = liste impossible a change
# dict = {} 

age = 14
firstname = "Tristan"
name = "Tabourier"


mylist  = [firstname, name] 
for elem in mylist:
    print(elem)

print(mylist)

myditct = {"luke": 5, "darth": 9,}
for key in myditct:
    print(key)

for key in myditct.values():
    print(key)

for key in myditct.items():
    print(key)

for k,v in myditct.items():
    myditct[k] = 5
    print(myditct[k])
